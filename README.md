## How to run

There is a Makefile with some helpers to get the project going

`make reset`

- Gets go dependencies
- Builds search binary
- Builds server binary

`make solr`

Runs a solr server with docker

`make create-solr-core`

The first time you run solr, you need to create a new core (like a database in the SQL world).
The server needs to be running for this to work.

`make search`

Builds the search script binary

`make serve`

Runs the HTTP server on port 5000

`make clean`

Deletes search binary

`make clean-solr`

Removes solr docs
