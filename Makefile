SOLR_CORE=searchy

reset: clean get build-bins

build-bins: dist/search dist/server

solr:
	docker run --rm --name solr -v $(shell pwd)/tmp/solrdata:/var/solr -p 8983:8983 solr:8-slim

create-solr-core:
	docker exec solr /opt/solr/bin/solr create -c ${SOLR_CORE}

clean-solr:
	curl "http://localhost:8983/solr/searchy/update?commit=true" -H "Content-Type: text/xml" --data-binary '<delete><query>*:*</query></delete>'

dist/search:
	go build -o ./dist/search ./cmd/run/main.go

dist/server:
	go build -o ./dist/server ./cmd/server/main.go

serve:
	dist/server

run-server:
	go run ./cmd/server/main.go

clean:
	rm -f ./dist/*

get:
	go get ./...