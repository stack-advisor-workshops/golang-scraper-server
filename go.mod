module github.com/mraaroncruz/scraper-cmd

go 1.15

replace github.com/mraaroncruz/scraper => ../scraper

require (
	github.com/labstack/echo/v4 v4.2.2
	github.com/mraaroncruz/scraper v0.0.0-00010101000000-000000000000
)
