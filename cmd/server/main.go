package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/mraaroncruz/scraper"
)

var searchClient *scraper.SearchClient

const coreName = "searchy"

func main() {
	buildSearchClient()

	e := echo.New()
	e.POST("/index", IndexView)
	e.GET("/search", SearchView)

	e.GET("/", LandingPageView)
	e.Logger.Fatal(e.Start(":5000"))
}

func buildSearchClient() {
	var err error
	searchClient, err = scraper.NewSearchClient(coreName)
	if err != nil {
		log.Fatal(err)
	}
}

func LandingPageView(c echo.Context) error {
	return c.String(http.StatusOK, "Scraper Spider Server Crawler")
}

func IndexView(c echo.Context) error {
	rootURL := c.QueryParam("url")
	siteID := c.QueryParam("site_id")

	page := scraper.Start(rootURL)
	fmt.Println("Got page")

	pages := page.ToPagelist()
	fmt.Println("Got page list")

	scraper.IndexPages("searchy", siteID, pages)
	fmt.Printf("Done indexing pages...\n")
	return c.JSON(http.StatusOK, map[string]int{"pageCount": len(pages)})
}

func SearchView(c echo.Context) error {
	query := c.QueryParam("query")
	docs, err := searchClient.Search("page", query)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	fmt.Printf("%#v\n", docs)
	return c.JSON(http.StatusOK, docs)
}
