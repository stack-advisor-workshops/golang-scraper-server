package main

import (
	"flag"
	"fmt"

	"github.com/mraaroncruz/scraper"
)

var siteID = flag.String("id", "", "The site's ID")
var rootURL = flag.String("root-url", "", "The site's root URL")

func main() {
	flag.Parse()
	fmt.Printf("Site ID: %s\n", *siteID)

	page := scraper.Start(*rootURL)
	fmt.Println("Got page")

	pages := page.ToPagelist()
	fmt.Println("Got page list")

	scraper.IndexPages("searchy", *siteID, pages)
	fmt.Printf("Done indexing pages...\n")
}
